export const fetchUser = (id) => {
  return fetch(
    `https://bsa-marketplace.azurewebsites.net/api/Products/${id}`,
    {
      method: "GET",
      headers: {
        "accept": "text/plain",
      },
    },
  );
};

export const fetchGoods = (text = '') => {
  return fetch(`https://bsa-marketplace.azurewebsites.net/api/Products?page=1&perPage=20&filter=${text}`);
};

export const fetchDeleteGoods = (id) => {
  return fetch(`https://bsa-marketplace.azurewebsites.net/api/Products/${id}`,
    {
      method: "DELETE",
      headers: {
        "accept": "*/*",
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImpvaG5AZ21haWwuY29tIiwibmFtZWlkIjoiNDAiLCJuYmYiOjE2NDMzNzQ3OTksImV4cCI6MTY0Njk3NDc5OSwiaWF0IjoxNjQzMzc0Nzk5fQ.ClK7MliG3Nqvpu0avX4BMzVjUnS1a2kMfisuStBCMlU",
      },
    },
  );
};

