import React, { useState, useEffect } from "react";
import { Alert, FlatList, View } from "react-native";
import SearchGoods from "../search/SearchGoods";
import GoodsItem from "../GoodsItem/GoodsItem";
import { styles } from "./home.styles";
import { fetchDeleteGoods, fetchGoods } from "../fetchData/fetchData";

const Home = ({ navigation }) => {
  const [goods, setGoods] = useState([]);
  const searchGoods = (text) => {
    fetchGoods(text)
      .then(response => response.json())
      .then(data => {
        if (data.length > 0) {
          setGoods(data);
        } else {
          Alert.alert(`No such goods - ${text}`);
        }
      })
      .catch(error => {
        console.log("Error : " + error);
      });
  };
  const getItemId = id => {
    navigation.navigate("ItemInfo", { id });
  };
  const handleDelete = id => {
    fetchDeleteGoods(id)
      .then(response => {
        if (response.ok) {
          const newGoods = goods.filter(el => el.id !== id);
          setGoods(newGoods);
        }
      })
      .catch(error => {
        console.log("Error : " + error);
      });
  };
  useEffect(() => {
    fetchGoods()
      .then(response => response.json())
      .then(data => setGoods(data))
      .catch(error => {
        console.log("Error : " + error);
      });
  }, []);
  return (
    <View style={styles.home}>
      <SearchGoods searchGoods={searchGoods} />
      <View style={styles.c}>
        <FlatList
          data={goods}
          renderItem={({ item }) => (
            <GoodsItem
              item={item}
              getItemId={() => getItemId(item.id)}
              handleDelete={() => handleDelete(item.id)}
            />
          )}
          keyExtractor={item => item.id}
        />
      </View>
    </View>
  );
};

export default Home;
