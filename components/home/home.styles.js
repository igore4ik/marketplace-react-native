import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  home: {
    padding: 5,
    margin: 5,
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
  },
  c: {
    marginTop: 50,
  },
  loader: {
    marginVertical: 15,
    alignItems: "center",
  },
});
