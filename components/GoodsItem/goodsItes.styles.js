import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  item: {
    backgroundColor: "#ececec",
    borderColor: "grey",
    borderWidth: 2,
    borderStyle: "solid",
    padding: 5,
    margin: 5,
    flexDirection: "row",
    alignItems: "center",
    width: "98%",
  },
  img: {
    width: 80,
    height: 80,
  },
  info: {
    width: "55%",
    marginLeft: 10,
    alignSelf:"stretch",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  desc: {
    fontSize: 13,
  },
  price: {
    alignSelf:"stretch",
    justifyContent: "space-between",
  },
  deleteBtn: {
    textTransform: "uppercase",
    backgroundColor: "#2196F3",
    color: "#fff",
    padding: 4,
    borderRadius: 5,
    fontWeight: "600",
  },
});
