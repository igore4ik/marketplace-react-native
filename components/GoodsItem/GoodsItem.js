import React from "react";
import { Text, View, Image, TouchableOpacity, Button } from "react-native";
import { styles } from "./goodsItes.styles";

const GoodsItem = ({ item, getItemId, handleDelete }) => {
  return (
    <TouchableOpacity>
      <View style={styles.item}>
        <Image
          style={styles.img}
          source={{
            uri: item.picture ? item.picture :
              "https://cdn.pixabay.com/photo/2013/07/13/10/45/cracks-157707_960_720.png"
          }}
        />
        <View style={styles.info}>
          <TouchableOpacity>
            <Text style={styles.title} onPress={getItemId}>
              {item.title}
            </Text>
          </TouchableOpacity>
          <Text style={styles.desc} numberOfLines={4}>
            {item.description}
          </Text>
        </View>
        <View style={styles.price}>
          <Text>{item.price}$</Text>
          <TouchableOpacity>
            <Text onPress={handleDelete} style={styles.deleteBtn} >Delete</Text>
            {/*<Button title={"Delete"} style={styles.deleteBtn} onPress={handleDelete}/>*/}
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default GoodsItem;
