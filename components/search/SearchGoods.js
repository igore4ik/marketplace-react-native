import React, { useState } from "react";
import { View, TextInput, Button } from "react-native";
import { styles } from "./search.styles";

const SearchGoods = ({searchGoods}) => {
  const [inputText, setInputText] = useState("");

  const submitHandler = (text)=> {
    searchGoods(text);
    setInputText('');
  };

  return (
    <View style={styles.formWrapper}>
      <TextInput
        style={styles.input}
        onChangeText={text => setInputText(text)}
        defaultValue={inputText}
      />
      <Button style={styles.btn} title="Find" onPress={()=>submitHandler(inputText)} />
    </View>
  );
};



export default SearchGoods;
