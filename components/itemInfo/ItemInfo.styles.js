import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    padding: 25,
    height: "100%",
    justifyContent: "space-between",
  },
  imgContainer: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    height: "30%",
  },
  img: {
    flex: 3,
    width: "100%",
    height: "100%",
  },
  titleContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    marginVertical: 5,
    fontSize: 15,
  },
  title: {
    fontSize: 18,
  },
  descContainer: {
    height: "25%",
  },
  seller: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  sellerImg: {
    width: 50,
    height: 50,
  },
  sellerInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "80%",
  },
});
