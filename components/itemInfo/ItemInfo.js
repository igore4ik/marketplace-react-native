import React, { useEffect, useState } from "react";
import {
  Linking,
  Button,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from "react-native";
import { styles } from "./ItemInfo.styles";
import { fetchUser } from "../fetchData/fetchData";

const ItemInfo = ({ route }) => {
  const [user, setUser] = useState(null);
  const id = route.params.id;

  useEffect(() => {
    fetchUser(id)
      .then(response => response.json())
      .then(data => setUser(data))
      .catch(error => console.log("Error : " + error));
  }, );

  return !user ? (
    <ActivityIndicator
      size="large"
      style={{ borderColor: "#aaa", alignItems: "center" }}
    />
  ) : (
    <View style={styles.container}>
      <View style={styles.imgContainer}>
        <TouchableOpacity>
          <Button title="prev" testID={"btnId"} />
        </TouchableOpacity>
        <Image
          source={{
            uri: user.pictures[0] != null ?
              user.pictures[0].url :
              "https://cdn.pixabay.com/photo/2013/07/13/10/45/cracks-157707_960_720.png",
          }}
          style={styles.img}
        />
        <TouchableOpacity>
          <Button title="next" />
        </TouchableOpacity>
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{user.title}</Text>
        <Text >{user.price}$</Text>
      </View>
      <View style={styles.descContainer}>
        <ScrollView>
          <View >
            <Text>{user.description}</Text>
          </View>
        </ScrollView>
      </View>
      <View style={styles.seller}>
        <Image source={{
          uri: user.seller.avatar != null ?
            user.seller.avatar :
            "https://cdn.pixabay.com/photo/2016/09/01/08/24/smiley-1635449_960_720.png",
        }}
               style={styles.sellerImg} />
        <View style={styles.sellerInfo}>
          <Text style={{ fontSize: 20 }}>{user.seller.name}</Text>
          <Text style={{ fontSize: 18 }}>{user.seller.phoneNumber}</Text>
        </View>
      </View>
      <View>
        <TouchableOpacity>
          <Button
            title="Call Seller"
            onPress={() => {
              Linking.openURL(`tel:${user.seller.phoneNumber}`);
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ItemInfo;
