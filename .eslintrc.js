module.exports = {
  root: true,
  extends: '@react-native-community',
  // extends: ['@react-native-community','prettier'],
  rules: {
    'prettier/prettier': 0,
    // 'quotes': 'off',
    'quotes': [ 0,'single'],
  },
};
